import re, bson
from collections import defaultdict

f = open("data.txt", "r")
data = f.read()

def remove_accents(input_str):
    s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹßñä'
    s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYybna'
    s = ''
    for c in input_str:
        if c in s1:
            s += s0[s1.index(c)]
        elif ord(c) not in range(760, 900) and ord(c) not in range(161, 191):
            s += c
    s = re.sub(r'\s+', ' ', s)
    return s.strip().lower()

def clean(inp):
    res = remove_accents(inp)
    res = re.sub('[^A-Za-z]+', ' ', res)
    while re.findall("\s\s", res):
        res = re.sub('\s\s', '\s', inp)
    return res.strip()

data = clean(data)

def extract(word):
    ue = '[ueoaiy]'
    results = []
    reg = re.findall(ue, word)
    if len(reg) > 0:
        start = word.index(reg[0])
        end = word.index(reg[-1])
        results.append(word[:start])
        results.append(word[start:end+1])
        results.append(word[end+1:])
    return results

def collectDict(data):
    dictionary = set()
    for d in data.split(" "):
        for e in extract(d):
            dictionary.add(e)
    dictionary.remove("")
    return dictionary

dictionary = collectDict(data)

def splitWord(word):
    ue = "ueoaiy"
    key = word[0] in ue
    results = []
    for w in word:
        if (w in ue) ^ key:
            results[-1] = results[-1] + w
        else :
            key = not key
            results.append(w)
    return results

def isVN(word, dic = dictionary):
    sp = splitWord(word)
    if len(sp) > 3:
        return False
    for s in sp:
        if s not in dic:
            return False
    num = 0
    for s in sp:
        if len(re.findall("[ueaoiy]", s)) > 0:
            num += 1
    if num != 1:
        return False
    if len(sp) == 3 and num == 1 and len(re.findall("[ueiaoy]", sp[1])) == 0:
        return False
    return True
