
# coding: utf-8

# In[1]:


import unicodedata as ud
latin_letters= {}

def is_latin(uchr):
    try: return latin_letters[uchr]
    except KeyError:
         return latin_letters.setdefault(uchr, 'LATIN' in ud.name(uchr))

def only_roman_chars(unistr):
    return all(is_latin(uchr)
           for uchr in unistr
           if uchr.isalpha())


# In[2]:


import string
def remove_strange_character(input_str):
    s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐÐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹßñä'
    s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYybna'
    s = ''
    for c in input_str:
        if c in s1:
            s += s0[s1.index(c)]
        elif c in string.punctuation or c.isdigit() or c == " ":
            s += c
        elif c.isalpha() and only_roman_chars(c):
            s += c
    return s


# In[3]:


def remove_accents(input_str):
    s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹßñä'
    s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYybna'
    s = ''
    for c in input_str:
        if c in s1:
            s += s0[s1.index(c)]
        else:
            s += c
    return s


# In[4]:


def remove_unword_character(inp):
    res = re.sub('[^A-Za-z]+', ' ', inp)
    while re.findall("\s\s", res):
        res = re.sub('\s\s', '\s', inp)
    return res.strip()


# In[20]:

import re
def clean(word):
    word = remove_strange_character(word)
    s = ""
    single = ""
    for i in range(len(word)):
        if word[i] in string.punctuation:
            single = ""
            if len(single) > 1:
                s += " "
            else: 
                try:
                    a = word[i+2]
                    if a.isalpha():
                        s += " "
                except IndexError:
                    pass
        elif word[i] == " ":
            single = ""
            s += " "
        else:
            single += word[i]
            s += word[i]
    s = re.sub('[\s]+', ' ', s)
    return s.strip()

